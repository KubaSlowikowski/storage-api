package pl.slowikowski.demo.synchronization.storage.productGroup;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.slowikowski.demo.synchronization.storage.abstraction.AbstractStorageEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product_groups_client")
@NoArgsConstructor
@Getter
@Setter
public class ProductGroupRevision extends AbstractStorageEntity {
    private String name;
    private String description;
//    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "group", fetch = FetchType.LAZY)
//    private Set<ProductRevision> products;
}
