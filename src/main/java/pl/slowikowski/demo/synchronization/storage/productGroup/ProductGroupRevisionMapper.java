package pl.slowikowski.demo.synchronization.storage.productGroup;

import org.mapstruct.Mapper;
import pl.slowikowski.demo.crud.productGroup.ProductGroupDTO;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonRevisionMapper;
import pl.slowikowski.demo.synchronization.storage.product.ProductRevisionMapper;

@Mapper(componentModel = "spring", uses = {ProductRevisionMapper.class})
public interface ProductGroupRevisionMapper extends CommonRevisionMapper<ProductGroupRevision, ProductGroupDTO> {
}
