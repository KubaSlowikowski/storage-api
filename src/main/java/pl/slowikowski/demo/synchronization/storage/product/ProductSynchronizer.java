package pl.slowikowski.demo.synchronization.storage.product;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import pl.slowikowski.demo.crud.product.ProductDTO;
import pl.slowikowski.demo.synchronization.storage.abstraction.AbstractStorageSynchronizer;

@Service
@ConditionalOnProperty(prefix = "storage.self-synchronization.", value = "enabled")
public class ProductSynchronizer extends AbstractStorageSynchronizer<ProductDTO, ProductRevision> {
    protected ProductSynchronizer(ProductClient client, ProductRevisionMapper mapper, ProductRevisionRepository repository) {
        super(client, mapper, repository);
    }
}
