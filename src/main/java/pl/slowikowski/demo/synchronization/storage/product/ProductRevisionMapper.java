package pl.slowikowski.demo.synchronization.storage.product;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueMappingStrategy;
import pl.slowikowski.demo.crud.product.ProductDTO;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonRevisionMapper;

@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public abstract class ProductRevisionMapper implements CommonRevisionMapper<ProductRevision, ProductDTO> {

    @AfterMapping
    protected void afterMappingFromDTO(ProductDTO source, @MappingTarget ProductRevision target) {
        if (source.getGroupId() == null || source.getGroupId() == 0) {
            target.setGroupId(1L);
        }
    }
}
