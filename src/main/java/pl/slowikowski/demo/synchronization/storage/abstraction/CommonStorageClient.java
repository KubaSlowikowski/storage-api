package pl.slowikowski.demo.synchronization.storage.abstraction;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.slowikowski.demo.crud.abstraction.AbstractDto;
import pl.slowikowski.demo.synchronization.AuditList;

public interface CommonStorageClient<D extends AbstractDto> {
    @GetMapping(path = "/{rev}")
    AuditList<D> getAuditList(@PathVariable("rev") Integer revisionId);
}
