package pl.slowikowski.demo.synchronization.storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.SneakyThrows;
import pl.slowikowski.demo.security.payload.response.JwtResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

public class StorageRestAuthInterceptor implements RequestInterceptor {
    private final Map<String, String> credentials;
    private final String url;
    private String token;

    public StorageRestAuthInterceptor(final String username, final String password, final String url) {
        this.url = url;
        credentials = new HashMap<>();
        credentials.put("username", username);
        credentials.put("password", password);
    }

    @Override
    @SneakyThrows
    public void apply(final RequestTemplate template) {

        if (isTokenValid()) {
            template.header("Authorization", "Bearer " + token);
            return;
        }

        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(credentials);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/auth/signin"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        ObjectMapper responseReader = new ObjectMapper();
        token = responseReader
                .reader()
                .readValue(response.body(), JwtResponse.class)
                .getToken();
        template.header("Authorization", "Bearer " + token);
    }

    @SneakyThrows
    private boolean isTokenValid() {
        if (token == null) {
            return false;
        }
        HttpRequest testAccessRequest = HttpRequest.newBuilder()
                .uri(URI.create(url + "/test/user"))
                .GET()
                .header("Authorization", "Bearer " + token)
                .build();
        HttpResponse<Void> response = HttpClient.newHttpClient().send(testAccessRequest, HttpResponse.BodyHandlers.discarding());
        return response.statusCode() == 200;
    }
}
