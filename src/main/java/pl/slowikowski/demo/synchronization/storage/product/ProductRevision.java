package pl.slowikowski.demo.synchronization.storage.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.slowikowski.demo.synchronization.storage.abstraction.AbstractStorageEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "products_client")
@NoArgsConstructor
@Getter
@Setter
public class ProductRevision extends AbstractStorageEntity {
    private String name;
    private String description;
    private int price;
    private boolean sold;
    private Long groupId;
}
