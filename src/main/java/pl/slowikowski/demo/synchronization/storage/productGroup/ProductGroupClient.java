package pl.slowikowski.demo.synchronization.storage.productGroup;

import org.springframework.cloud.openfeign.FeignClient;
import pl.slowikowski.demo.crud.productGroup.ProductGroupDTO;
import pl.slowikowski.demo.synchronization.storage.StorageRestClientConfig;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonStorageClient;

@FeignClient(
        value = "productGroupClient",
        url = "${storage.product-groups.database.sync.url}",
        configuration = StorageRestClientConfig.class,
        fallbackFactory = ProductGroupClientFallbackFactory.class)
public interface ProductGroupClient extends CommonStorageClient<ProductGroupDTO> {
}
