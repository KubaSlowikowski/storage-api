package pl.slowikowski.demo.synchronization.storage.abstraction;

import pl.slowikowski.demo.crud.abstraction.AbstractDto;

import java.util.List;
import java.util.Set;

public interface CommonRevisionMapper<E extends AbstractStorageEntity, D extends AbstractDto> {

    D toDto(E e);

    E fromDto(D dto);

    Set<D> toSetDto(Set<E> e);

    Set<E> fromSetDto(Set<D> d);

    List<D> toListDto(List<E> e);

    List<E> fromListDto(List<D> d);
}
