package pl.slowikowski.demo.synchronization.storage.abstraction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface CommonStorageRepository<E extends AbstractStorageEntity> extends JpaRepository<E, Long>, RevisionRepository<E, Long, Long> {
    default Integer getMaxRev() { //FIXME
        return null;
    }
}
