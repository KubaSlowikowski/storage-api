package pl.slowikowski.demo.synchronization.storage.productGroup;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import pl.slowikowski.demo.crud.productGroup.ProductGroupDTO;
import pl.slowikowski.demo.synchronization.storage.abstraction.AbstractStorageSynchronizer;

@Service
@ConditionalOnProperty(prefix = "storage.self-synchronization.", value = "enabled")
public class ProductGroupSynchronizer extends AbstractStorageSynchronizer<ProductGroupDTO, ProductGroupRevision> {
    protected ProductGroupSynchronizer(ProductGroupClient client, ProductGroupRevisionMapper mapper, ProductGroupRevisionRepository commonRepository) {
        super(client, mapper, commonRepository);
    }
}
