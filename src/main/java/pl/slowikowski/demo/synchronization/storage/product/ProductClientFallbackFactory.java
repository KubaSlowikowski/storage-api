package pl.slowikowski.demo.synchronization.storage.product;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.slowikowski.demo.crud.product.ProductDTO;
import pl.slowikowski.demo.synchronization.AuditList;

@Component
class ProductClientFallbackFactory implements FallbackFactory<ProductClient> {
    private static final Logger logger = LoggerFactory.getLogger(ProductClientFallbackFactory.class);

    @Override
    public ProductClient create(final Throwable cause) {
        return new ProductClient() {
            @Override
            public AuditList<ProductDTO> getAuditList(final Integer revisionId) {
                onError();
                return null;
            }

            private void onError() {
                logger.error("Product client fallback. Reason was: " + cause.getClass() + ". Cause: " + cause.getMessage());
            }
        };
    }
}
