package pl.slowikowski.demo.synchronization.storage.productGroup;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.slowikowski.demo.crud.productGroup.ProductGroupDTO;
import pl.slowikowski.demo.synchronization.AuditList;

@Component
class ProductGroupClientFallbackFactory implements FallbackFactory<ProductGroupClient> {
    private static final Logger logger = LoggerFactory.getLogger(ProductGroupClientFallbackFactory.class);

    @Override
    public ProductGroupClient create(final Throwable cause) {
        return new ProductGroupClient() {
            @Override
            public AuditList<ProductGroupDTO> getAuditList(final Integer revisionId) {
                onError();
                return null;
            }

            private void onError() {
                logger.error("Product-group client fallback. Reason was: " + cause.getClass() + ". Cause: " + cause.getMessage());
            }
        };
    }
}
