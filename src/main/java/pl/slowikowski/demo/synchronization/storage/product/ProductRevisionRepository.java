package pl.slowikowski.demo.synchronization.storage.product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonStorageRepository;

@Repository
public interface ProductRevisionRepository extends CommonStorageRepository<ProductRevision> {
    @Override
    @Query("SELECT max(e.revisionId) FROM ProductRevision e")
    Integer getMaxRev();
}
