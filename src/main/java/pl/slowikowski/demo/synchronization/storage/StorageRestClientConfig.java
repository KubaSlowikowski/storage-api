package pl.slowikowski.demo.synchronization.storage;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

//@Configuration
@NoArgsConstructor
public class StorageRestClientConfig {
    @Value("${storage.auth.username}") //TODO replace @Value with properties object
    private String username;
    @Value("${storage.auth.password}")
    private String password;
    @Value("${storage.api.url}")
    private String apiUrl;

    @Bean
    public StorageRestAuthInterceptor storageRestAuthInterceptor() {
        return new StorageRestAuthInterceptor(username, password, apiUrl);
    }
}
