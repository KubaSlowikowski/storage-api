package pl.slowikowski.demo.synchronization.storage.abstraction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import pl.slowikowski.demo.crud.abstraction.AbstractDto;
import pl.slowikowski.demo.synchronization.AuditList;

import java.util.List;

public abstract class AbstractStorageSynchronizer<D extends AbstractDto, E extends AbstractStorageEntity> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractStorageSynchronizer.class);
    private final CommonStorageClient<D> client;
    private final CommonRevisionMapper<E, D> mapper;
    private final CommonStorageRepository<E> commonRepository;
    private Integer lastRevision;

    protected AbstractStorageSynchronizer(final CommonStorageClient<D> client, final CommonRevisionMapper<E, D> mapper, final CommonStorageRepository<E> commonRepository) {
        this.client = client;
        this.mapper = mapper;
        this.commonRepository = commonRepository;
        this.lastRevision = commonRepository.getMaxRev();
        if (lastRevision == null) {
            lastRevision = 0;
        }
    }

    @Scheduled(fixedDelay = 5000)
    private void sync() {
        AuditList<D> auditList = client.getAuditList(lastRevision);

        lastRevision = auditList.getLastRevisionId();
        if (lastRevision == null) {
            lastRevision = 0;
        }

        List<E> modified = mapper.fromListDto(auditList.getModified());
        modified.forEach(entity -> entity.setRevisionId(lastRevision));
        commonRepository.saveAll(modified);

        for (Long id : auditList.getDeleted()) {
            try {
                commonRepository.deleteById(id);
            } catch (EmptyResultDataAccessException ignored) {
            }
        }
    }
}
