package pl.slowikowski.demo.synchronization.storage.productGroup;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonStorageRepository;

@Repository
public interface ProductGroupRevisionRepository extends CommonStorageRepository<ProductGroupRevision> {
    @Override
    @Query("SELECT max(e.revisionId) FROM ProductGroupRevision e")
    Integer getMaxRev();
}
