package pl.slowikowski.demo.synchronization.storage.abstraction;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public class AbstractStorageEntity {
    @Id
    private Long id;
    private Integer revisionId;
}
