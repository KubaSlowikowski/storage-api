package pl.slowikowski.demo.synchronization.storage.product;

import org.springframework.cloud.openfeign.FeignClient;
import pl.slowikowski.demo.crud.product.ProductDTO;
import pl.slowikowski.demo.synchronization.storage.StorageRestClientConfig;
import pl.slowikowski.demo.synchronization.storage.abstraction.CommonStorageClient;

@FeignClient(
        value = "productClient",
        url = "${storage.products.database.sync.url}",
        configuration = StorageRestClientConfig.class,
        fallbackFactory = ProductClientFallbackFactory.class)
public interface ProductClient extends CommonStorageClient<ProductDTO> {
}
