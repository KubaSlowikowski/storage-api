package pl.slowikowski.demo.synchronization.library.book;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.synchronization.library.abstraction.AbstractLibraryEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "books_client")
@Getter
@Setter
public class Book extends AbstractLibraryEntity {
    private String author;
    private String title;
    private int publicationYear;
    private Long readerId;
}
