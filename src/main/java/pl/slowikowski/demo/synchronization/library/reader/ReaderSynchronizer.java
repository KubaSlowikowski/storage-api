package pl.slowikowski.demo.synchronization.library.reader;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import pl.slowikowski.demo.feign_client.dto.ReaderDTO;
import pl.slowikowski.demo.feign_client.rest_client.reader.ReaderClient;
import pl.slowikowski.demo.synchronization.library.abstraction.AbstractLibrarySynchronizer;

@Service
@ConditionalOnProperty(prefix = "storage.library.synchronize.", value = "enabled")
public class ReaderSynchronizer extends AbstractLibrarySynchronizer<ReaderDTO, Reader> {

    protected ReaderSynchronizer(ReaderClient client, ReaderMapper mapper, ReaderRepository repository) {
        super(client, mapper, repository);
    }
}
