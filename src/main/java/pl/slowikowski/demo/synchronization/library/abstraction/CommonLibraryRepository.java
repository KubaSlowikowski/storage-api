package pl.slowikowski.demo.synchronization.library.abstraction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface CommonLibraryRepository<E extends AbstractLibraryEntity> extends JpaRepository<E, Long>, RevisionRepository<E, Long, Long> {
    default Integer getMaxRev() { //FIXME
        return null;
    }
}
