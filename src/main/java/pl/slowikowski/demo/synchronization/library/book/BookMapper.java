package pl.slowikowski.demo.synchronization.library.book;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.slowikowski.demo.feign_client.dto.BookDTO;
import pl.slowikowski.demo.synchronization.library.abstraction.CommonLibraryMapper;

@Mapper(componentModel = "spring")
public abstract class BookMapper implements CommonLibraryMapper<BookDTO, Book> {
    @Override
    @Mapping(source = "reader.id", target = "readerId")
    public abstract Book fromDto(BookDTO dto);
}
