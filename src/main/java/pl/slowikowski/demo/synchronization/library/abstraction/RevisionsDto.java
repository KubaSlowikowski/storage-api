package pl.slowikowski.demo.synchronization.library.abstraction;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.feign_client.dto.AbstractLibraryDTO;

import java.util.List;

@Getter
@Setter
public class RevisionsDto<D extends AbstractLibraryDTO> {

    private List<D> createdEntities;
    private List<D> deletedEntities;
    private List<D> updatedEntities;
    private Integer lastRevisionId;

}
