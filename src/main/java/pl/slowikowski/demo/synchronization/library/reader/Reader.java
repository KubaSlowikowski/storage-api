package pl.slowikowski.demo.synchronization.library.reader;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.synchronization.library.abstraction.AbstractLibraryEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "readers_client")
@Getter
@Setter
public class Reader extends AbstractLibraryEntity {
    private String firstName;
    private String lastName;
    private String email;
    private String role;
    private String username;
}
