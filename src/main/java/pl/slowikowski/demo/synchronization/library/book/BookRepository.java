package pl.slowikowski.demo.synchronization.library.book;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.slowikowski.demo.synchronization.library.abstraction.CommonLibraryRepository;

@Repository
public interface BookRepository extends CommonLibraryRepository<Book> {
    @Override
    @Query("SELECT max(e.revisionId) FROM Book e")
    Integer getMaxRev();
}
