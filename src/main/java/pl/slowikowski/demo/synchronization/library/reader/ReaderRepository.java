package pl.slowikowski.demo.synchronization.library.reader;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.slowikowski.demo.synchronization.library.abstraction.CommonLibraryRepository;

@Repository
public interface ReaderRepository extends CommonLibraryRepository<Reader> {
    @Override
    @Query("SELECT max(e.revisionId) FROM Reader e")
    Integer getMaxRev();
}
