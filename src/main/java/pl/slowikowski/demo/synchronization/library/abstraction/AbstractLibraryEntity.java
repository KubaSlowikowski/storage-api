package pl.slowikowski.demo.synchronization.library.abstraction;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public class AbstractLibraryEntity {
    @Id
    private Long id;
    private Integer revisionId;
}
