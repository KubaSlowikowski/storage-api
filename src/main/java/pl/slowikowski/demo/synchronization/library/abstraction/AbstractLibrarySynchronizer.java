package pl.slowikowski.demo.synchronization.library.abstraction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import pl.slowikowski.demo.feign_client.dto.AbstractLibraryDTO;
import pl.slowikowski.demo.feign_client.rest_client.abstraction.CommonLibraryClient;

import java.util.List;

public abstract class AbstractLibrarySynchronizer<D extends AbstractLibraryDTO, E extends AbstractLibraryEntity> {
    public static final Logger logger = LoggerFactory.getLogger(AbstractLibrarySynchronizer.class);
    private final CommonLibraryClient<D> client;
    private final CommonLibraryMapper<D, E> mapper;
    private final CommonLibraryRepository<E> repository;
    private Integer lastRevision;

    protected AbstractLibrarySynchronizer(final CommonLibraryClient<D> client, final CommonLibraryMapper<D, E> mapper, final CommonLibraryRepository<E> repository) {
        this.client = client;
        this.mapper = mapper;
        this.repository = repository;
        this.lastRevision = repository.getMaxRev();
        if (lastRevision == null) {
            lastRevision = 0;
        }
    }

    @Scheduled(fixedDelay = 10000)
    private void sync() {
        RevisionsDto<D> revisions = client.getRevisions(lastRevision);

        lastRevision = revisions.getLastRevisionId();
        if (lastRevision == null) {
            lastRevision = 0;
        }

        List<E> created = mapper.fromListDto(revisions.getCreatedEntities());
        created.forEach(entity -> entity.setRevisionId(lastRevision));
        repository.saveAll(created);

        List<E> updated = mapper.fromListDto(revisions.getUpdatedEntities());
        updated.forEach(entity -> entity.setRevisionId(lastRevision));
        repository.saveAll(updated);

        List<E> deleted = mapper.fromListDto(revisions.getDeletedEntities());
        for (E entity : deleted) {
            try {
                repository.deleteById(entity.getId());
            } catch (EmptyResultDataAccessException e) {
            }
        }
        logger.info("Synchronized library data.");
    }
}
