package pl.slowikowski.demo.synchronization.library.abstraction;

import pl.slowikowski.demo.feign_client.dto.AbstractLibraryDTO;

import java.util.List;
import java.util.Set;

public interface CommonLibraryMapper<D extends AbstractLibraryDTO, E extends AbstractLibraryEntity> {
    D toDto(E e);

    E fromDto(D dto);

    Set<D> toSetDto(Set<E> e);

    Set<E> fromSetDto(Set<D> d);

    List<D> toListDto(List<E> e);

    List<E> fromListDto(List<D> d);
}
