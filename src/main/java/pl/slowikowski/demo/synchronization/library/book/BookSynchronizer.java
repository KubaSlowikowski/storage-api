package pl.slowikowski.demo.synchronization.library.book;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import pl.slowikowski.demo.feign_client.dto.BookDTO;
import pl.slowikowski.demo.feign_client.rest_client.book.BookClient;
import pl.slowikowski.demo.synchronization.library.abstraction.AbstractLibrarySynchronizer;

@Service
@ConditionalOnProperty(prefix = "storage.library.synchronize.", value = "enabled")
public class BookSynchronizer extends AbstractLibrarySynchronizer<BookDTO, Book> {

    protected BookSynchronizer(BookClient client, BookMapper mapper, BookRepository repository) {
        super(client, mapper, repository);
    }
}
