package pl.slowikowski.demo.synchronization.library.reader;

import org.mapstruct.Mapper;
import pl.slowikowski.demo.feign_client.dto.ReaderDTO;
import pl.slowikowski.demo.synchronization.library.abstraction.CommonLibraryMapper;

@Mapper(componentModel = "spring")
public abstract class ReaderMapper implements CommonLibraryMapper<ReaderDTO, Reader> {
}
