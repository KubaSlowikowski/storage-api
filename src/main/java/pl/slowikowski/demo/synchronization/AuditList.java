package pl.slowikowski.demo.synchronization;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.crud.abstraction.AbstractDto;

import java.util.List;

@Getter
@Setter
public class AuditList<D extends AbstractDto> {

    private List<D> modified;
    private List<Long> deleted;
    private Integer lastRevisionId;
}
