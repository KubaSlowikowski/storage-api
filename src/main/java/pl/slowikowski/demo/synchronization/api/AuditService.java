package pl.slowikowski.demo.synchronization.api;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.slowikowski.demo.crud.abstraction.AbstractDto;
import pl.slowikowski.demo.crud.abstraction.AbstractEntity;
import pl.slowikowski.demo.crud.abstraction.CommonMapper;
import pl.slowikowski.demo.crud.exception.WrongIdException;
import pl.slowikowski.demo.synchronization.AuditList;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Service
public class AuditService<E extends AbstractEntity, D extends AbstractDto> {
    private static final Logger logger = LoggerFactory.getLogger(AuditService.class);
    private final EntityManager entityManager;

    public AuditService(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public AuditList<D> getAuditList(Integer revisionId, Class<? extends AbstractEntity> clazz, CommonMapper<E, D> mapper) {
        if (revisionId < 0) {
            throw new WrongIdException(Long.valueOf(revisionId));
        }
        AuditReader auditReader = AuditReaderFactory.get(entityManager);

        AuditList<D> result = new AuditList<>();
        result.setModified(mapper.toListDto(getModified(revisionId, clazz, auditReader)));
        result.setDeleted(getDeleted(revisionId, clazz, auditReader));
        result.setLastRevisionId(getLastRevisionId(clazz, auditReader));

        logger.info("Synchonized data. " + clazz.getSimpleName());
        return optimizeAuditList(result);
    }

    private Integer getLastRevisionId(Class<? extends AbstractEntity> clazz, AuditReader auditReader) {
        return (Integer) auditReader.createQuery().forRevisionsOfEntity(clazz, true, true)
                .addProjection(AuditEntity.revisionNumber().max())
                .getSingleResult();
    }

    private List<Long> getDeleted(Integer revisionId, Class<? extends AbstractEntity> clazz, AuditReader auditReader) {
        AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(clazz, true, true);
        return (List<Long>) query
                .add(AuditEntity.revisionType().eq(RevisionType.DEL))
                .add(AuditEntity.revisionNumber().gt(revisionId))
                .addProjection(AuditEntity.id())
                .getResultList();
    }

    private List<E> getModified(Integer revisionId, Class<? extends AbstractEntity> clazz, AuditReader auditReader) {
        AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(clazz, true, false);
        return (List<E>) query
                .add(AuditEntity.revisionNumber().gt(revisionId))
                .addOrder(AuditEntity.revisionNumber().asc())
                .getResultList();
    }

    private AuditList<D> optimizeAuditList(AuditList<D> auditList) {
        List<Long> toDelete = new ArrayList<>(auditList.getDeleted());
        List<D> modified = auditList.getModified();

        for (int i = modified.size() - 1; i >= 0; i--) {
            Long id = modified.get(i).getId();
            if (toDelete.contains(id)) {
                modified.remove(i);
            }
        }
        auditList.setModified(modified);
        return auditList;
    }
}
