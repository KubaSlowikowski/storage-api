package pl.slowikowski.demo.feign_client.dto;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.crud.abstraction.AbstractDto;

@Getter
@Setter
public class AbstractLibraryDTO extends AbstractDto {
}
