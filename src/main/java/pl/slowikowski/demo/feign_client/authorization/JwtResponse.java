package pl.slowikowski.demo.feign_client.authorization;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.slowikowski.demo.feign_client.dto.AbstractLibraryDTO;

@Getter
@Setter
@NoArgsConstructor
public class JwtResponse extends AbstractLibraryDTO {
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String type;
    private String[] roles;
    private String token;
}
