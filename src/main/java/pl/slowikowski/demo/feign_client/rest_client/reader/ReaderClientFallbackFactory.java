package pl.slowikowski.demo.feign_client.rest_client.reader;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.slowikowski.demo.feign_client.dto.ReaderDTO;
import pl.slowikowski.demo.synchronization.library.abstraction.RevisionsDto;
import pl.slowikowski.demo.utils.CustomPageImpl;

@Component
class ReaderClientFallbackFactory implements FallbackFactory<ReaderClient> {
    private static final Logger logger = LoggerFactory.getLogger(ReaderClientFallbackFactory.class);

    @Override
    public ReaderClient create(final Throwable cause) {
        return new ReaderClient() {
            @Override
            public CustomPageImpl<ReaderDTO> findAll(final Pageable page, final String search) {
                onError();
                return null;
            }

            @Override
            public ReaderDTO create(final ReaderDTO dto) {
                onError();
                return null;
            }

            @Override
            public ReaderDTO getById(final Long id) {
                onError();
                return null;
            }

            @Override
            public ReaderDTO update(final ReaderDTO dto, final Long id) {
                onError();
                return null;
            }

            @Override
            public ReaderDTO delete(final Long id) {
                onError();
                return null;
            }

            @Override
            public RevisionsDto<ReaderDTO> getRevisions(final Integer lastRevisionId) {
                onError();
                return null;
            }

            private void onError() {
                logger.error("Book client fallback. Reason was: " + cause.getClass() + ". Cause: " + cause.getMessage());
            }
        };
    }
}
