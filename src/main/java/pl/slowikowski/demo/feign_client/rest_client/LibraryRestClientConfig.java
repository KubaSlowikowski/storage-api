package pl.slowikowski.demo.feign_client.rest_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import pl.slowikowski.demo.feign_client.LibraryProperties;

//@Configuration
public class LibraryRestClientConfig {
    @Autowired
    private LibraryProperties libraryProperties;
    @Value("${library.auth.url}")
    private String authUrl;

    @Bean
    public LibraryRestAuthInterceptor libraryRestAuthInterceptor() {
        return new LibraryRestAuthInterceptor(libraryProperties, authUrl);
    }
}
