package pl.slowikowski.demo.feign_client.rest_client.book;

import org.springframework.cloud.openfeign.FeignClient;
import pl.slowikowski.demo.feign_client.dto.BookDTO;
import pl.slowikowski.demo.feign_client.rest_client.LibraryRestClientConfig;
import pl.slowikowski.demo.feign_client.rest_client.abstraction.CommonLibraryClient;

@FeignClient(
        value = "book",
        url = "${library.url}/books",
        configuration = LibraryRestClientConfig.class,
        fallbackFactory = BookClientFallbackFactory.class)
public interface BookClient extends CommonLibraryClient<BookDTO> {
}
