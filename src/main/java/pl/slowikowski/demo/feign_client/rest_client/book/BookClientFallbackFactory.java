package pl.slowikowski.demo.feign_client.rest_client.book;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.slowikowski.demo.feign_client.dto.BookDTO;
import pl.slowikowski.demo.synchronization.library.abstraction.RevisionsDto;
import pl.slowikowski.demo.utils.CustomPageImpl;

@Component
class BookClientFallbackFactory implements FallbackFactory<BookClient> {
    private static final Logger logger = LoggerFactory.getLogger(BookClientFallbackFactory.class);

    @Override
    public BookClient create(final Throwable cause) {
        return new BookClient() {

            @Override
            public CustomPageImpl<BookDTO> findAll(final Pageable page, final String search) {
                onError();
                return null;
            }

            @Override
            public BookDTO create(final BookDTO dto) {
                onError();
                return null;
            }

            @Override
            public BookDTO getById(final Long id) {
                onError();
                return null;
            }

            @Override
            public BookDTO update(final BookDTO dto, final Long id) {
                onError();
                return null;
            }

            @Override
            public BookDTO delete(final Long id) {
                onError();
                return null;
            }

            @Override
            public RevisionsDto<BookDTO> getRevisions(final Integer lastRevisionId) {
                onError();
                return null;
            }

            private void onError() {
                logger.error("Book client fallback. Reason was: " + cause.getClass() + ". Cause: " + cause.getMessage());
            }
        };
    }

}
