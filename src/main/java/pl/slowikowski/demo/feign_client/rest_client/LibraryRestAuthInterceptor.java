package pl.slowikowski.demo.feign_client.rest_client;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.SneakyThrows;
import pl.slowikowski.demo.feign_client.LibraryProperties;
import pl.slowikowski.demo.feign_client.authorization.JwtResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

public class LibraryRestAuthInterceptor implements RequestInterceptor { //feign clients requests only
    private final Map<String, String> credentials;
    private final String url;
    private String token;


    public LibraryRestAuthInterceptor(final LibraryProperties libraryProperties, final String url) {
        this.url = url;
        credentials = new HashMap<>();
        credentials.put("username", libraryProperties.getUsername());
        credentials.put("password", libraryProperties.getPassword());
    }

    @Override
    @SneakyThrows
    public void apply(RequestTemplate template) {
        if (isTokenValid()) {
            template.header("Authorization", "Bearer " + token);
            return;
        }

        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(credentials);


        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        ObjectMapper responseReader = new ObjectMapper();
        token = responseReader
                .reader()
                .readValue(response.body(), JwtResponse.class)
                .getToken();
        template.header("Authorization", "Bearer " + token);
    }

    private boolean isTokenValid() {
        if (token == null) {
            return false;
        }
        return false; //TODO until library API will implement access endpoints
//        HttpRequest testAccessRequest = HttpRequest.newBuilder()
//                .uri(URI.create(url + "/test/user"))
//                .GET()
//                .header("Authorization", "Bearer " + token)
//                .build();
//        HttpResponse<Void> response = HttpClient.newHttpClient().send(testAccessRequest, HttpResponse.BodyHandlers.discarding());
//        return response.statusCode() == 200;
    }
}
