package pl.slowikowski.demo.crud.abstraction;

import org.springframework.security.access.prepost.PreAuthorize;
import pl.slowikowski.demo.export.ExportDto;
import pl.slowikowski.demo.export.excel.ExcelGenerator;
import pl.slowikowski.demo.export.pdf.PdfGenerator;

import java.util.List;

public interface Exportable<D extends AbstractDto> {

    @PreAuthorize("hasRole('USER')")
    default ExportDto toPdfReport(List<D> dtos, String fileName) {
        return PdfGenerator.build(dtos, fileName);
    }

    @PreAuthorize("hasRole('USER')")
    default ExportDto toXlsxReport(List<D> dtos, String fileName) {
        return ExcelGenerator.build(dtos, fileName);
    }
}
