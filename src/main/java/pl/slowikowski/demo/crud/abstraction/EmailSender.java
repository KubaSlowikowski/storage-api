package pl.slowikowski.demo.crud.abstraction;

import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.prepost.PreAuthorize;
import pl.slowikowski.demo.email.Message;

import java.util.concurrent.CompletableFuture;

interface EmailSender {
    @PreAuthorize("hasRole('USER')")
    @Async
    CompletableFuture<String> sendAllInMail(Pageable pageable, String search, Message message);
}
