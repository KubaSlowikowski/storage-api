package pl.slowikowski.demo.crud.abstraction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.slowikowski.demo.email.Message;
import pl.slowikowski.demo.export.ExportDto;
import pl.slowikowski.demo.synchronization.AuditList;

import javax.validation.Valid;
import java.time.LocalDate;

@CrossOrigin(origins = "${storage.front.url}", allowCredentials = "true")
public abstract class AbstractController<T extends CommonService<D>, D extends AbstractDto> {

    private final T service;

    public AbstractController(T service) {
        this.service = service;
    }

    @GetMapping
    Page<D> findAll(@PageableDefault Pageable pageable, @RequestParam(value = "search", required = false) String search) {
        return service.getAll(pageable, search);
    }

    @GetMapping(path = "/{id}")
    D findById(@PathVariable("id") Long id) {
        return service.findById(id);
    }

    @PostMapping
    D save(@RequestBody @Valid D dto) {
        return service.save(dto);
    }

    @PutMapping(path = "/{id}")
    D update(@PathVariable("id") Long id, @RequestBody @Valid D dto) {
        return service.update(id, dto);
    }

    @DeleteMapping(path = "/{id}")
    D delete(@PathVariable("id") Long id) {
        return service.delete(id);
    }

    @GetMapping(path = "/pdf", produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<byte[]> getPdf(@PageableDefault Pageable pageable, @RequestParam(value = "search", required = false) String search, @RequestParam(value = "fileName", required = false) String fileName) {
        ExportDto pdfReport = service.toPdf(pageable, search, fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + pdfReport.getFileName() + "_" + LocalDate.now() + pdfReport.getFileExtension().getExtension())
                .body(pdfReport.getByteArray());
    }

    @GetMapping(path = "/excel")
    ResponseEntity<byte[]> getExcel(@PageableDefault Pageable pageable, @RequestParam(value = "search", required = false) String search, @RequestParam(value = "fileName", required = false) String fileName) {
        ExportDto pdfReport = service.toXlsx(pageable, search, fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + pdfReport.getFileName() + "_" + LocalDate.now() + pdfReport.getFileExtension().getExtension())
                .body(pdfReport.getByteArray());
    }

    @GetMapping(path = "/email")
    ResponseEntity<Void> sendAllInMail(@PageableDefault Pageable pageable, @RequestParam(value = "search", required = false) String search, @ModelAttribute @Valid Message message) {
        service.sendAllInMail(pageable, search, message);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/sync/{rev}")
    AuditList<D> getAuditList(@PathVariable("rev") Integer revisionId) {
        return service.getAuditList(revisionId);
    }
}
