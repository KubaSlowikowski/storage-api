package pl.slowikowski.demo.crud.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.slowikowski.demo.crud.abstraction.AbstractEntity;
import pl.slowikowski.demo.crud.abstraction.AbstractService;
import pl.slowikowski.demo.crud.exception.NotFoundException;
import pl.slowikowski.demo.crud.productGroup.ProductGroup;
import pl.slowikowski.demo.crud.productGroup.ProductGroupRepository;
import pl.slowikowski.demo.email.EmailService;
import pl.slowikowski.demo.synchronization.api.AuditService;

import java.util.List;

@Service
public class ProductServiceImpl extends AbstractService<Product, ProductDTO> implements ProductService {

    private final ProductRepository repository;
    private final ProductGroupRepository groupRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(final ProductRepository repository, final ProductGroupRepository groupRepository, final ProductMapper productMapper, final EmailService emailService, final AuditService<Product, ProductDTO> auditService) {
        super(productMapper, repository, emailService, auditService);
        this.repository = repository;
        this.groupRepository = groupRepository;
        this.productMapper = productMapper;
    }

    @Override
    @Transactional
    public Page<ProductDTO> findAllByGroupId(Long groupId, Pageable page) {
        groupRepository.findById(groupId).orElseThrow(() -> new NotFoundException(groupId, ProductGroup.class.getSimpleName()));
        List<Product> productList = repository.findAllByGroup_Id(groupId);
        return new PageImpl<>(productMapper.toListDto(productList), page, productList.size());
    }

    @Override
    @Transactional
    public ProductDTO buyProduct(Long id) {
        ProductDTO productDto = findById(id);
        productDto.toogle();
        Product product = productMapper.fromDto(productDto);
        var result = repository.saveAndFlush(product);
        return productMapper.toDto(result);
    }

    @Override
    protected Class<? extends AbstractEntity> getEntityClass() {
        return Product.class;
    }
}
