package pl.slowikowski.demo.crud.product;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import pl.slowikowski.demo.crud.abstraction.CommonMapper;
import pl.slowikowski.demo.crud.productGroup.ProductGroupRepository;

@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public abstract class ProductMapper implements CommonMapper<Product, ProductDTO> {
    public static ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);
    @Autowired
    private ProductGroupRepository groupRepository;

    @Override
    @Mapping(source = "group.id", target = "groupId")
    public abstract ProductDTO toDto(Product product);

    @Override
    @Mapping(source = "groupId", target = "group.id")
    public abstract Product fromDto(ProductDTO dto);

    @AfterMapping
    protected void afterMappingFromDTO(ProductDTO source, @MappingTarget Product target) {
        if (source.getGroupId() == null || source.getGroupId() == 0) {
            target.setGroup(groupRepository.findById(1L).get());
        }
    }
}
