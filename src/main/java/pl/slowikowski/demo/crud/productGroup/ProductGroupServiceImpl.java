package pl.slowikowski.demo.crud.productGroup;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.slowikowski.demo.crud.abstraction.AbstractEntity;
import pl.slowikowski.demo.crud.abstraction.AbstractService;
import pl.slowikowski.demo.crud.exception.GroupModifyingForbiddenException;
import pl.slowikowski.demo.crud.product.ProductMapper;
import pl.slowikowski.demo.crud.product.ProductRepository;
import pl.slowikowski.demo.email.EmailService;
import pl.slowikowski.demo.synchronization.api.AuditService;

@Service
public class ProductGroupServiceImpl extends AbstractService<ProductGroup, ProductGroupDTO> implements ProductGroupService {

    private final ProductGroupRepository groupRepository;
    private final ProductRepository productRepository;
    private final ProductGroupMapper groupMapper;

    public ProductGroupServiceImpl(final ProductGroupRepository groupRepository, final ProductRepository productRepository, final ProductGroupMapper groupMapper, final ProductMapper productMapper, final EmailService emailService, final AuditService<ProductGroup, ProductGroupDTO> auditService) {
        super(groupMapper, groupRepository, emailService, auditService);
        this.groupRepository = groupRepository;
        this.productRepository = productRepository;
        this.groupMapper = groupMapper;
    }

    @Override
    @Transactional
    public ProductGroupDTO save(ProductGroupDTO dto) {
        if (dto.getId() != null && dto.getId() == 1) {
            throw new GroupModifyingForbiddenException();
        }
        ProductGroup entity = groupMapper.fromDto(dto);
        ProductGroup savedResult = groupRepository.saveAndFlush(entity);
        if (entity.getProducts() != null) {
            entity.getProducts().forEach(productRepository::saveAndFlush);
        }
        return groupMapper.toDto(savedResult);
    }

    @Override
    @Transactional
    public ProductGroupDTO update(Long id, ProductGroupDTO toUpdate) {
        if (id == 1) {
            throw new GroupModifyingForbiddenException();
        }
        toUpdate.setId(id);

        ProductGroup systemGroup = getEntityById(1L);
        ProductGroup previousVersionOfGroup = getEntityById(id);

        previousVersionOfGroup
                .getProducts()
                .forEach(product -> product.setGroup(systemGroup));

        ProductGroup result = groupMapper.fromDto(toUpdate);
        groupRepository.save(result);
        return groupMapper.toDto(result);
    }

    @Override
    @Transactional
    public ProductGroupDTO delete(Long id) {
        if (id == 1) {
            throw new GroupModifyingForbiddenException();
        }
//        productRepository.assignProductsFromGroupWithIdToSystemGroup(id);
        ProductGroup defaultGroup = groupRepository.findById(1L).get();
        productRepository.findAllByGroup_Id(id).forEach(product -> {
            product.setGroup(defaultGroup);
            productRepository.save(product);
        });


        ProductGroupDTO dto = findById(id);
        groupRepository.deleteById(dto.getId());
        return dto;
    }

    @Override
    protected Class<? extends AbstractEntity> getEntityClass() {
        return ProductGroup.class;
    }
}
