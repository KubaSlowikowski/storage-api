package pl.slowikowski.demo.export;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.slowikowski.demo.utils.FileExtension;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExportDto {
    private String fileName = "testName"; //FIXME
    private FileExtension fileExtension;
    private byte[] byteArray;
}
