package pl.slowikowski.demo.export.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.SneakyThrows;
import pl.slowikowski.demo.export.ExportDto;
import pl.slowikowski.demo.export.pdf.tableConfig.TableConfigFactory;
import pl.slowikowski.demo.utils.FileExtension;

import java.io.ByteArrayOutputStream;
import java.util.Collection;

public class PdfGenerator {

    private PdfGenerator() {
    }

    @SneakyThrows
    public static ExportDto build(final Collection<?> rows, String fileName) {
        PdfPTable pdfPTable = TableBuilder.build(rows);
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Class<?> clazz = rows.iterator().next().getClass();
        String tableTitle = TableConfigFactory.getTable(clazz).getTitle();

        PdfWriter.getInstance(document, out);
        document.open();
        document.addTitle(tableTitle);
        document.add(new Phrase(tableTitle, FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK)));
        document.add(Chunk.NEWLINE);
        document.add(pdfPTable);
        document.close();

        if (fileName == null) {
            fileName = clazz.getSimpleName();
        }

        return new ExportDto(fileName, FileExtension.PDF, out.toByteArray());
    }
}
