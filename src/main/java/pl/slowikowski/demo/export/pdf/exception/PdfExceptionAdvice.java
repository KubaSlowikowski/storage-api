package pl.slowikowski.demo.export.pdf.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class PdfExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(PdfEmptyCollectionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String pdfEmptyCollectionExceptionHandler(PdfEmptyCollectionException e) {
        return e.getMessage();
    }
}
