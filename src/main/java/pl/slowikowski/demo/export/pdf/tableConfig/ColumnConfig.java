package pl.slowikowski.demo.export.pdf.tableConfig;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import lombok.SneakyThrows;
import pl.slowikowski.demo.export.pdf.annotation.PdfColumn;

import java.lang.reflect.Field;

public class ColumnConfig {

    private Field field;
    private String name;

    private PdfPCell header;
    private PdfPCell cell;

    private Font headerFont = new Font();
    private Font cellFont = new Font();
    private PdfColumn columnProperties;

    ColumnConfig(Field field) {
        this.field = field;
        this.header = new PdfPCell();
        this.cell = new PdfPCell();
        this.columnProperties = field.getAnnotation(PdfColumn.class);

        field.setAccessible(true);
        setColumnStyle();
    }

    private void setColumnStyle() {
        setColumnName();
        setFonts();
        setBackgrounds();
    }

    private void setColumnName() {
        if (columnProperties != null && !columnProperties.name().isEmpty()) {
            this.name = columnProperties.name();
        } else {
            this.name = field.getName();
        }
    }

    private void setFonts() {
        if (columnProperties != null) {

            headerFont = FontFactory.getFont(
                    columnProperties
                            .headerFont()
                            .getFont()
                            .getBaseFont()
                            .getPostscriptFontName(),
                    columnProperties.headerTextSize(),
                    columnProperties.headerTextColor().getColor()
            );
            cellFont = FontFactory.getFont(
                    columnProperties
                            .columnFont()
                            .getFont()
                            .getBaseFont()
                            .getPostscriptFontName(),
                    columnProperties.columnTextSize(),
                    columnProperties.columnTextColor().getColor()
            );
        } else {
            headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            cellFont = FontFactory.getFont(FontFactory.TIMES_ITALIC, 11, BaseColor.BLACK);
        }

        header.setHorizontalAlignment(Element.ALIGN_CENTER);
        header.setVerticalAlignment(Element.ALIGN_CENTER);
        header.setBorderWidth(1);

        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(10);
    }

    private void setBackgrounds() {
        if (columnProperties != null) {
            header.setBackgroundColor(columnProperties.headerBackgroundColor().getColor());
            cell.setBackgroundColor(columnProperties.columnBackgroundColor().getColor());
        } else {
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        }
    }

    public PdfPCell getHeader() {
        Phrase phrase = new Phrase(name, headerFont);
        header.setPhrase(phrase);
        return header;
    }

    public PdfPCell getCell(Object row) {
        Phrase phrase = new Phrase(getRowValue(row), cellFont);
        cell.setPhrase(phrase);
        return cell;
    }

    @SneakyThrows
    private String getRowValue(Object row) {
        Object value = field.get(row);
        if (value == null) {
            return "";
        }
        return value.toString();
    }
}
