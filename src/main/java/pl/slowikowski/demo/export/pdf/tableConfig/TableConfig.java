package pl.slowikowski.demo.export.pdf.tableConfig;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.export.pdf.annotation.PdfIgnoreField;
import pl.slowikowski.demo.export.pdf.annotation.PdfTableName;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TableConfig {

    private String title;
    private List<ColumnConfig> columnList;

    public TableConfig(final Class<?> clazz) {
        this.title = getTitle(clazz);
        this.columnList = getColumnBuilder(clazz);
    }

    private static String getTitle(final Class<?> clazz) {
        if (!clazz.isAnnotationPresent(PdfTableName.class)) {
            return "";
        } else {
            PdfTableName pdfTableName = clazz.getAnnotation(PdfTableName.class);
            return pdfTableName.value();
        }
    }

    private static List<ColumnConfig> getColumnBuilder(final Class<?> clazz) {
        Class<?> superclass = clazz.getSuperclass();

        Field[] fields = clazz.getDeclaredFields();
        Field[] superclassFields = superclass.getDeclaredFields();
        List<ColumnConfig> columns = new ArrayList<>();

        for (Field field : superclassFields) {
            if (!field.isAnnotationPresent(PdfIgnoreField.class)) {
                columns.add(new ColumnConfig(field));
            }
        }

        for (Field field : fields) {
            if (!field.isAnnotationPresent(PdfIgnoreField.class)) {
                columns.add(new ColumnConfig(field));
            }
        }
        return columns;
    }
}
