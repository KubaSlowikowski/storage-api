package pl.slowikowski.demo.export.pdf.annotation;

import pl.slowikowski.demo.export.pdf.utils.ColorEnum;
import pl.slowikowski.demo.export.pdf.utils.FontEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PdfColumn {
    String name() default "";

    ColorEnum headerBackgroundColor() default ColorEnum.LIGHT_GRAY;

    ColorEnum columnBackgroundColor() default ColorEnum.WHITE;

    ColorEnum headerTextColor() default ColorEnum.BLACK;

    ColorEnum columnTextColor() default ColorEnum.BLACK;

    FontEnum headerFont() default FontEnum.HELVETICA_BOLD;

    FontEnum columnFont() default FontEnum.TIMES_ITALIC;

    float headerTextSize() default -1.0F;

    float columnTextSize() default 11.0F;
}
