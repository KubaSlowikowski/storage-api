package pl.slowikowski.demo.export.pdf;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import pl.slowikowski.demo.export.pdf.exception.PdfEmptyCollectionException;
import pl.slowikowski.demo.export.pdf.tableConfig.ColumnConfig;
import pl.slowikowski.demo.export.pdf.tableConfig.TableConfigFactory;

import java.util.Collection;
import java.util.List;

class TableBuilder {

    static PdfPTable build(final Collection<?> rows) {

        if (rows.isEmpty()) {
            throw new PdfEmptyCollectionException();
        }

        final List<ColumnConfig> columns = TableConfigFactory.getTable(rows.iterator().next().getClass()).getColumnList();
        PdfPTable pdfTable = new PdfPTable(columns.size());

        for (ColumnConfig columnConfig : columns) {
            PdfPCell cell = new PdfPCell(columnConfig.getHeader());
            pdfTable.addCell(cell);
        }

        for (Object row : rows) {
            for (ColumnConfig columnConfig : columns) {
                PdfPCell cell = columnConfig.getCell(row);
                pdfTable.addCell(cell);
            }
        }
        pdfTable.setWidthPercentage(100);
        return pdfTable;
    }
}
