package pl.slowikowski.demo.export.pdf.utils;

import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import lombok.Getter;

@Getter
public enum FontEnum {
    COURIER(FontFactory.getFont(FontFactory.COURIER)),
    COURIER_BOLD(FontFactory.getFont(FontFactory.COURIER_BOLD)),
    COURIER_OBLIQUE(FontFactory.getFont(FontFactory.COURIER_OBLIQUE)),
    COURIER_BOLDOBLIQUE(FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE)),
    HELVETICA(FontFactory.getFont(FontFactory.HELVETICA)),
    HELVETICA_BOLD(FontFactory.getFont(FontFactory.HELVETICA_BOLD)),
    HELVETICA_OBLIQUE(FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE)),
    HELVETICA_BOLDOBLIQUE(FontFactory.getFont(FontFactory.HELVETICA_BOLDOBLIQUE)),
    SYMBOL(FontFactory.getFont(FontFactory.SYMBOL)),
    TIMES(FontFactory.getFont(FontFactory.TIMES)),
    TIMES_ROMAN(FontFactory.getFont(FontFactory.TIMES_ROMAN)),
    TIMES_BOLD(FontFactory.getFont(FontFactory.TIMES_BOLD)),
    TIMES_ITALIC(FontFactory.getFont(FontFactory.TIMES_ITALIC)),
    TIMES_BOLDITALIC(FontFactory.getFont(FontFactory.TIMES_BOLDITALIC)),
    ZAPFDINGBATS(FontFactory.getFont(FontFactory.ZAPFDINGBATS));

    private Font font;

    FontEnum(final Font font) {
        this.font = font;
    }
}
