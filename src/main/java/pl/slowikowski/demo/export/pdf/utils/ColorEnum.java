package pl.slowikowski.demo.export.pdf.utils;

import com.itextpdf.text.BaseColor;
import lombok.Getter;

@Getter
public enum ColorEnum {
    WHITE(BaseColor.WHITE),
    CYAN(BaseColor.CYAN),
    BLUE(BaseColor.BLUE),
    BLACK(BaseColor.BLACK),
    GREEN(BaseColor.GREEN),
    GRAY(BaseColor.GRAY),
    LIGHT_GRAY(BaseColor.LIGHT_GRAY),
    RED(BaseColor.RED),
    YELLOW(BaseColor.YELLOW);

    BaseColor color;

    ColorEnum(BaseColor color) {
        this.color = color;
    }
}
