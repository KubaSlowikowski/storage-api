package pl.slowikowski.demo.export.pdf.exception;

public class PdfEmptyCollectionException extends RuntimeException {

    public PdfEmptyCollectionException() {
        super("Can't build table from an empty collection");
    }
}
