package pl.slowikowski.demo.export.pdf.tableConfig;

import java.util.HashMap;
import java.util.Map;

public class TableConfigFactory { //flyweight design pattern

    private static final Map<Class<?>, TableConfig> factoryTableConfig = new HashMap<>();

    public static TableConfig getTable(Class<?> clazz) {
        TableConfig tableConfig = factoryTableConfig.get(clazz);

        if (tableConfig == null) {
            tableConfig = new TableConfig(clazz);
            factoryTableConfig.put(clazz, tableConfig);
        }

        return tableConfig;
    }
}
