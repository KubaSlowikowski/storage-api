package pl.slowikowski.demo.export.excel;

import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Workbook;
import pl.slowikowski.demo.export.ExportDto;
import pl.slowikowski.demo.utils.FileExtension;

import java.io.ByteArrayOutputStream;
import java.util.Collection;

public class ExcelGenerator {

    private ExcelGenerator() {
    }

    @SneakyThrows
    public static ExportDto build(final Collection<?> rows, String fileName) {
        Workbook workbook = WorkbookBuilder.build(rows);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        Class<?> clazz = rows.iterator().next().getClass();
        if (fileName == null) {
            fileName = clazz.getSimpleName();
        }

        return new ExportDto(fileName, FileExtension.EXCEL, out.toByteArray());
    }
}
