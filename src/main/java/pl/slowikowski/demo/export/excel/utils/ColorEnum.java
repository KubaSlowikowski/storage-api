package pl.slowikowski.demo.export.excel.utils;

import lombok.Getter;
import org.apache.poi.ss.usermodel.IndexedColors;

@Getter
public enum ColorEnum {
    BLACK(IndexedColors.BLACK),
    WHITE(IndexedColors.WHITE),
    GREY(IndexedColors.GREY_80_PERCENT),
    LIGHT_GREY(IndexedColors.GREY_25_PERCENT),
    GREEN(IndexedColors.GREEN),
    BLUE(IndexedColors.BLUE),
    YELLOW(IndexedColors.YELLOW),
    ORANGE(IndexedColors.ORANGE),
    LIGHT_ORANGE(IndexedColors.LIGHT_ORANGE),
    RED(IndexedColors.RED);
    /* and muuch mooore colors available in IndexedColors... */

    IndexedColors color;

    ColorEnum(IndexedColors color) {
        this.color = color;
    }
}
