package pl.slowikowski.demo.export.excel.exception;

public class ExcelEmptyCollectionException extends RuntimeException {

    public ExcelEmptyCollectionException() {
        super("Can't build workbook from an empty collection");
    }
}
