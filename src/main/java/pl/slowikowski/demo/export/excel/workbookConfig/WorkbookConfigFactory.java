package pl.slowikowski.demo.export.excel.workbookConfig;

import java.util.HashMap;
import java.util.Map;

public class WorkbookConfigFactory {

    private static final Map<Class<?>, WorkbookConfig> factoryWorkbookConfig = new HashMap<>();

    public static WorkbookConfig getWorkbookConfig(final Class<?> clazz) {
        WorkbookConfig workbookConfig = factoryWorkbookConfig.get(clazz);

        if (workbookConfig == null) {
            workbookConfig = new WorkbookConfig(clazz);
            factoryWorkbookConfig.put(clazz, workbookConfig);
        }

        return workbookConfig;
    }
}
