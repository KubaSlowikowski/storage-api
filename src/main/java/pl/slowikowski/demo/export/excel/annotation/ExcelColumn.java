package pl.slowikowski.demo.export.excel.annotation;


import pl.slowikowski.demo.export.excel.utils.ColorEnum;
import pl.slowikowski.demo.export.excel.utils.FontEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelColumn {
    String name() default "";

    ColorEnum headerBackgroundColor() default ColorEnum.LIGHT_ORANGE;

    ColorEnum columnBackgroundColor() default ColorEnum.LIGHT_GREY;

    FontEnum headerFont() default FontEnum.ARIAL;

    FontEnum columnFont() default FontEnum.ARIAL;
}
