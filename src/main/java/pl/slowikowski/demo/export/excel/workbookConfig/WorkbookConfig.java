package pl.slowikowski.demo.export.excel.workbookConfig;

import lombok.Getter;
import lombok.Setter;
import pl.slowikowski.demo.export.excel.annotation.ExcelIgnoreField;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class WorkbookConfig {

    private String title;
    private List<ColumnConfig> columnList;

    public WorkbookConfig(final Class<?> clazz) {
        this.title = getTitle(clazz);
        this.columnList = getColumnBuilder(clazz);
    }

    private String getTitle(final Class<?> clazz) {
        return clazz.getSimpleName(); //FIXME
    }

    private List<ColumnConfig> getColumnBuilder(final Class<?> clazz) {
        Class<?> superclass = clazz.getSuperclass();

        Field[] fields = clazz.getDeclaredFields();
        Field[] superclassFields = superclass.getDeclaredFields();
        List<ColumnConfig> columns = new ArrayList<>();

        for (Field field : superclassFields) {
            if (!field.isAnnotationPresent(ExcelIgnoreField.class)) {
                columns.add(new ColumnConfig(field));
            }
        }

        for (Field field : fields) {
            if (!field.isAnnotationPresent(ExcelIgnoreField.class)) {
                columns.add(new ColumnConfig(field));
            }
        }
        return columns;
    }
}
