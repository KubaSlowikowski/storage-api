package pl.slowikowski.demo.export.excel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ExcelExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(ExcelEmptyCollectionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String excelEmptyCollectionExceptionHandler(ExcelEmptyCollectionException e) {
        return e.getMessage();
    }
}
