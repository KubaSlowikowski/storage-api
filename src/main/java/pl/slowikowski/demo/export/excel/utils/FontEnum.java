package pl.slowikowski.demo.export.excel.utils;

import lombok.Getter;

@Getter
public enum FontEnum {
    ARIAL("Arial");

    String fontName;

    FontEnum(String fontName) {
        this.fontName = fontName;
    }
}
