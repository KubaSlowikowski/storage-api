package pl.slowikowski.demo.export.excel.workbookConfig;

import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.*;
import pl.slowikowski.demo.export.excel.annotation.ExcelColumn;

import java.lang.reflect.Field;

public class ColumnConfig {

    private Field field;
    private String name;

    private Cell header;
    private Cell cell;

    private Font headerFont;
    private Font cellFont;

    private CellStyle headerStyle;
    private CellStyle cellStyle;

    private ExcelColumn columnProperties;

    ColumnConfig(final Field field) {
        this.field = field;
        this.columnProperties = field.getAnnotation(ExcelColumn.class);

        field.setAccessible(true);
        setColumnName();
    }

    private void setColumnName() {
        if (columnProperties != null && !columnProperties.name().isEmpty()) {
            this.name = columnProperties.name();
        } else {
            this.name = field.getName();
        }
    }

    public void setHeaderFont(Font font) {
        headerFont = font;
        if (columnProperties != null) {
            headerFont.setFontName(columnProperties.headerFont().getFontName());
        }
        headerFont.setBold(true);
    }

    public void setHeaderStyle(CellStyle style) {
        headerStyle = style;
        headerStyle.setFont(headerFont);
        if (columnProperties != null) {
            headerStyle.setFillForegroundColor(columnProperties.headerBackgroundColor().getColor().getIndex());
        } else {
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        }
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        setBorder(headerStyle);
    }

    public void setCellFont(Font font) {
        cellFont = font;
        if (columnProperties != null) {
            cellFont.setFontName(columnProperties.columnFont().getFontName());
        }
    }

    public void setColumnStyle(CellStyle style) {
        cellStyle = style;
        if (columnProperties != null) {
            cellStyle.setFillForegroundColor(columnProperties.columnBackgroundColor().getColor().getIndex());
        } else {
            cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        }
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        setBorder(cellStyle);
    }

    private static void setBorder(CellStyle cellStyle) {
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
    }

    public void fillHeader(Cell cell) {
        cell.setCellStyle(headerStyle);
        cell.setCellValue(this.name);
    }

    @SneakyThrows
    public void fillCell(Cell cell, Object row) {
        String value = String.valueOf(field.get(row));
        if (value.equals("null")) {
            cell.setCellValue("");
        } else {
            cell.setCellValue(value);
        }
        cell.setCellStyle(cellStyle);
    }
}
