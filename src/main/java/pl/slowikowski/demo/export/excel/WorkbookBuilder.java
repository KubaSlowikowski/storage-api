package pl.slowikowski.demo.export.excel;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pl.slowikowski.demo.export.excel.exception.ExcelEmptyCollectionException;
import pl.slowikowski.demo.export.excel.workbookConfig.ColumnConfig;
import pl.slowikowski.demo.export.excel.workbookConfig.WorkbookConfig;
import pl.slowikowski.demo.export.excel.workbookConfig.WorkbookConfigFactory;

import java.util.Collection;

class WorkbookBuilder {

    static Workbook build(final Collection<?> rows) {

        if (rows.isEmpty()) {
            throw new ExcelEmptyCollectionException();
        }

        final WorkbookConfig workbookConfig = WorkbookConfigFactory.getWorkbookConfig(rows.iterator().next().getClass());
        Workbook workbook = new XSSFWorkbook();
        CreationHelper creationHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet(workbookConfig.getTitle());

        // Row for headers
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < workbookConfig.getColumnList().size(); i++) {
            ColumnConfig column = workbookConfig.getColumnList().get(i);
            column.setCellFont(workbook.createFont());
            column.setColumnStyle(workbook.createCellStyle());

            column.setHeaderFont(workbook.createFont());
            column.setHeaderStyle(workbook.createCellStyle());
            column.fillHeader(headerRow.createCell(i));

        }

        Object[] rowsArray = rows.toArray();
        for (int rowIndex = 0; rowIndex < rows.size(); rowIndex++) {
            Row row = sheet.createRow(rowIndex + 1);
            for (int columnIndex = 0; columnIndex < workbookConfig.getColumnList().size(); columnIndex++) {
                ColumnConfig column = workbookConfig.getColumnList().get(columnIndex);
                column.fillCell(row.createCell(columnIndex), rowsArray[rowIndex]);
            }
        }

        for (int i = 0; i < workbookConfig.getColumnList().size(); i++) {
            sheet.autoSizeColumn(i);
        }

        return workbook;
    }
}
