package pl.slowikowski.demo.utils;

import lombok.Getter;

@Getter
public enum FileExtension {
    PDF(".pdf"),
    EXCEL(".xlsx");

    private String extension;

    FileExtension(String extension) {
        this.extension = extension;
    }
}
