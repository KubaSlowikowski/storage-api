package pl.slowikowski.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pl.slowikowski.demo.crud.productGroup.ProductGroup;
import pl.slowikowski.demo.crud.productGroup.ProductGroupRepository;
import pl.slowikowski.demo.security.Roles;
import pl.slowikowski.demo.security.entity.Role;
import pl.slowikowski.demo.security.repository.RoleRepository;
import pl.slowikowski.demo.security.repository.UserRepository;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class DataLoader implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(DataLoader.class);
    private final ProductGroupRepository groupRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    @Value("${storage.api.url}")
    private String apiUrl;

    DataLoader(final ProductGroupRepository groupRepository, final RoleRepository roleRepository, final UserRepository userRepository) {
        this.groupRepository = groupRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        addDefaultGroup();
        addRoles();
        addUser();
    }

    private void addDefaultGroup() {
        if (!groupRepository.existsById(1L)) {
            ProductGroup defaultGroup = ProductGroup.ProductGroupBuilder.aProductGroup()
                    .withId(1L)
                    .withName("SYSTEM")
                    .withDescription("SYSTEM")
                    .build();
            groupRepository.save(defaultGroup);
            logger.info("Saved default group.");
        }
    }

    private void addRoles() {
        List<Role> roles = List.of(new Role(Roles.ROLE_ADMIN), new Role(Roles.ROLE_MODERATOR), new Role(Roles.ROLE_USER));
        if (roleRepository.findByName(Roles.ROLE_ADMIN).isEmpty() && roleRepository.findByName(Roles.ROLE_MODERATOR).isEmpty() && roleRepository.findByName(Roles.ROLE_USER).isEmpty()) {
            roleRepository.saveAll(roles);
            logger.info("Saved default roles.");
        }
    }

    @SneakyThrows
    private void addUser() {
        if (userRepository.findById(1L).isEmpty()) {
            Map<String, String> credentials = new HashMap<>(4);
            credentials.put("username", "admin");
            credentials.put("password", "12345678");
            credentials.put("email", "admin@email.com");
            credentials.put("role", "admin");

            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(credentials);

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(apiUrl + "/auth/signup"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();
            HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.discarding());

            logger.info("Saved admin account.");
        }
    }
}
