create table readers_client
(
    id          bigserial PRIMARY KEY,
    first_name  varchar(255) null,
    last_name   varchar(255) null,
    email       varchar(255) null,
    role        varchar(255) null,
    username    varchar(255) null,
    revision_id integer
);

create table books_client
(
    id               bigserial PRIMARY KEY,
    author           varchar(255) null,
    title            varchar(255) null,
    publication_year integer      null,
    reader_id        bigint,
    revision_id      integer
);
