create table revinfo
(
    rev      integer primary key,
    revtstmp bigserial
);

create table products_aud
(
    id               bigserial not null,
    rev              integer   not null,
    revtype          smallint,
    name             varchar(255),
    description      varchar(255),
    price            integer,
    sold             boolean,
    product_group_id bigserial,
    primary key (id, rev),
    constraint fk_products_aud_rev foreign key (rev) references revinfo (rev)
);

create table product_groups_aud
(
    id          bigserial not null,
    rev         integer   not null,
    revtype     smallint,
    description varchar(255),
    name        varchar(255),
    primary key (id, rev),
    constraint fk_product_groups_aud_rev foreign key (rev) references revinfo (rev)
);

create sequence public.hibernate_sequence;

alter table products_aud
    alter column product_group_id drop not null;
