create table product_groups_client
(
    id          bigserial PRIMARY KEY,
    name        varchar(100) null,
    description varchar(100) null,
    revision_id integer
);

create table products_client
(
    id          bigserial PRIMARY KEY,
    name        varchar(100) null,
    description varchar(100) null,
    price       integer      null,
    sold        boolean      null,
    group_id    bigint,
    revision_id integer
);
